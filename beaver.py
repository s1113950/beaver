import pygame, random, sys
from pygame.locals import *

import globalVars

def terminate():
	pygame.quit()
	sys.exit()

def waitForPlayerToPressKey():
	while True:
		for event in pygame.event.get():
			if event.type == QUIT:
				terminate()
			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:	#if hit escape
					terminate()
				return

def playerHasHitBaddie(playerRect, baddies):
	for baddie in baddies:
		if playerRect.colliderect(b['rect']):
			return True
	return False

def drawText(text, font, surface, x, y):
	textobj = font.render(text, 1, TEXTCOLOR)
	textrect = textobj.get_rect()
	textrect.topleft = (x, y)	#put it from top left
	surface.blit(textobj, textrect)	#draw it, and where

def Game():
	pygame.init()
	mainClock = pygame.time.Clock()
	windowSurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
	pygame.display.set_caption('Beaver Epicness')
	pygame.mouse.set_visible(False)	#Pff... who needs the mouse

	#set up fonts
	font = pygame.font.SysFont(None, 48)	#default font at 48 pt

	#set up sounds
	#gameOverSound = pygame.mixer.Sound('gameover.wav')
	#pygame.mixer.music.load('background.mid')

	#set up images
	playerImage = pygame.image.load('beaver.png')
	playerRect = playerImage.get_rect()
	baddieImage = pygame.image.load('baddie.png')	#change this later/add more

	#show "Start" screen
	drawText("Beaver Time!", font, windowSurface, (WINDOWWIDTH, WINDOWHEIGHT))
	drawText("Press a key to start.", font, windowSurface, (WINDOWWIDTH, WINDOWHEIGHT))
	pygame.display.update()	#print to screen
	waitForPlayerToPressKey()

	#start of game

if __name__ == '__main__':
	Game()